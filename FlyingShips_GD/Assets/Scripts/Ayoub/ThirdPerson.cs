﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPerson : MonoBehaviour {

    [SerializeField]
    private float m_MouseSensitivity;

    [SerializeField]
    private Transform m_Player;

    [SerializeField]
    private float m_DistanceFromTarget;

    [SerializeField]
    private float m_RotationTime;

    private Vector3 m_RotationVelocity;

    private Vector3 m_CurrentRotation;

    private Vector2 m_PitchMinMax = new Vector2(-40, 85);

    private float m_Yaw;
    private float m_Pitch;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        m_Yaw += Input.GetAxis("Mouse X") * m_MouseSensitivity;
        m_Pitch -= Input.GetAxis("Mouse Y") * m_MouseSensitivity;

        m_Pitch = Mathf.Clamp(m_Pitch, m_PitchMinMax.x, m_PitchMinMax.y);

        m_CurrentRotation = Vector3.SmoothDamp(m_CurrentRotation, new Vector3(m_Pitch, m_Yaw), ref m_RotationVelocity, m_RotationTime);
        
        transform.eulerAngles = m_CurrentRotation;

        transform.position = m_Player.position - transform.forward * m_DistanceFromTarget;
    }
}
