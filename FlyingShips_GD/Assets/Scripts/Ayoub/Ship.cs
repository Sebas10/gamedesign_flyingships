﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    [SerializeField]
    private float m_AccelSpeed;

    [SerializeField]
    private float m_TurnSpeed;

    private Rigidbody m_RigidBody;

	// Use this for initialization
	void Start ()
    {
        m_RigidBody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        float Horizontal = Input.GetAxis("Horizontal");
        float Vertical = Input.GetAxis("Vertical");

        float movement = 0f;
        movement = Mathf.Lerp(movement, Vertical, Time.deltaTime);
        transform.Translate(0f, 0f, movement * m_AccelSpeed);

        float steer = 0f;
        steer = Mathf.Lerp(steer, Horizontal, Time.deltaTime);
        transform.Rotate(0f, steer * m_TurnSpeed, 0f);

        //VelocityCap();
    }
}
