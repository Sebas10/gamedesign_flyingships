﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField]
    private GameObject m_NameTag;

    public bool m_IsSpotted;
    
	void Start ()
    {
        m_IsSpotted = false;

        m_NameTag.SetActive(m_IsSpotted);
	}
	
	// Update is called once per frame
	void Update ()
    {
        m_NameTag.SetActive(m_IsSpotted);

        if (Input.GetKeyDown(KeyCode.F))
        {
            if (m_IsSpotted == false)
            {
                m_IsSpotted = true;
            }
            else if(m_IsSpotted == true)
            {
                m_IsSpotted = false;
            }
        }

        Debug.Log(m_IsSpotted);
	}
}