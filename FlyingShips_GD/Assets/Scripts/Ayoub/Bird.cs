﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum BirdState
{
    Resting,
    Scouting,
    Attacking
}

public class Bird : MonoBehaviour
{
    [SerializeField]
    private Transform m_BoatLocation;
    private List<Transform> m_Waypoints = new List<Transform>();

    private float m_Speed;
    private float m_ScoutingArea;

    private BirdState m_State;

    [SerializeField]
    private AudioSource m_BirdSoundSource;

    [SerializeField]
    private AudioClip m_BirdSound;

    [SerializeField]
    private TextMeshProUGUI m_StateText;

	// Use this for initialization
	void Start ()
    {
        m_State = BirdState.Resting;

        m_BirdSoundSource.clip = m_BirdSound;
	}
	
	// Update is called once per frame
	void Update ()
    {
        switch (m_State)
        {
            case (BirdState.Resting):

                Resting();

                break;

            case (BirdState.Scouting):

                Scouting();

                break;

            case (BirdState.Attacking):

                // The bird is attacking a enemy.

                break;
        }

        Debug.Log(m_State);
	}

    private void Resting()
    {
        transform.position = m_BoatLocation.position;

        m_StateText.text = "Bird is resting";

        if (Input.GetKeyDown(KeyCode.F))
        {
            m_State = BirdState.Scouting;

            m_BirdSoundSource.Play();
        }
    }

    private void Scouting()
    {
        transform.position = new Vector3(m_BoatLocation.position.x + 150, m_BoatLocation.position.y, m_BoatLocation.position.z);

        m_StateText.text = "Bird is scouting the area";

        if (Input.GetKeyDown(KeyCode.F))
        {
            m_State = BirdState.Resting;

            m_BirdSoundSource.Play();
        }
    }

    private void Attacking()
    {

    }
}

