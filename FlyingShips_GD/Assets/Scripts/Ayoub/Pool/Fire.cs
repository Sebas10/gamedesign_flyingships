﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour {
    
    public ObjectPool m_BulletPool;

    [SerializeField]
    private float m_MaxFireRate;

    private float m_CurFireRate;

    [SerializeField]
    private KeyCode m_FireKey;

    private void Start()
    {
        m_CurFireRate = m_MaxFireRate;
    }

    void Update ()
    {
        m_CurFireRate += Time.deltaTime;

        if (Input.GetKeyDown(m_FireKey))
        {
            if (m_CurFireRate >= m_MaxFireRate)
            {
                m_BulletPool.InstantiateObject(transform.position, transform.rotation);

                m_CurFireRate = 0;
            }
        }
	}
}