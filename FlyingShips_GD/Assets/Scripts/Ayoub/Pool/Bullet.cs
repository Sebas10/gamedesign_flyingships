﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : PoolItem {

    [SerializeField]
    private float m_BulletSpeed;

    private float m_DeathTimer;

    protected override void Restart()
    {
        m_DeathTimer = 0.0f;
    }
    
    void Update ()
    {
        transform.Translate((Vector3.right * -1) * Time.deltaTime * m_BulletSpeed);

        m_DeathTimer += Time.deltaTime;

        if(m_DeathTimer >= 7.5f)
        {
            ReturnToPool();
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {

        }
    }
}
